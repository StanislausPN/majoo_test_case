class MovieDetails {
  Image? image;
  int? runningTimeInMinutes;
  String? nextEpisode;
  int? numberOfEpisodes;
  int? seriesEndYear;
  int? seriesStartYear;
  String? title;
  String? titleType;
  int? year;

  MovieDetails(
      {this.image,
      this.runningTimeInMinutes,
      this.nextEpisode,
      this.numberOfEpisodes,
      this.seriesEndYear,
      this.seriesStartYear,
      this.title,
      this.titleType,
      this.year});

  MovieDetails.fromJson(Map<String, dynamic> json) {
    image = json['image'] != null ? new Image.fromJson(json['image']) : null;
    runningTimeInMinutes = json['runningTimeInMinutes'];
    nextEpisode = json['nextEpisode'];
    numberOfEpisodes = json['numberOfEpisodes'];
    seriesEndYear = json['seriesEndYear'];
    seriesStartYear = json['seriesStartYear'];
    title = json['title'];
    titleType = json['titleType'];
    year = json['year'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.image != null) {
      data['image'] = this.image!.toJson();
    }
    data['runningTimeInMinutes'] = this.runningTimeInMinutes;
    data['nextEpisode'] = this.nextEpisode;
    data['numberOfEpisodes'] = this.numberOfEpisodes;
    data['seriesEndYear'] = this.seriesEndYear;
    data['seriesStartYear'] = this.seriesStartYear;
    data['title'] = this.title;
    data['titleType'] = this.titleType;
    data['year'] = this.year;
    return data;
  }
}

class Image {
  int? height;
  String? id;
  String? url;
  int? width;

  Image({this.height, this.id, this.url, this.width});

  Image.fromJson(Map<String, dynamic> json) {
    height = json['height'];
    id = json['id'];
    url = json['url'];
    width = json['width'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['height'] = this.height;
    data['id'] = this.id;
    data['url'] = this.url;
    data['width'] = this.width;
    return data;
  }
}
