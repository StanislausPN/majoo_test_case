import 'dart:convert';

class User {
  String? email;
  String? userName;
  String? password;

  User({this.email, this.userName, this.password});

  User.fromJson(Map<String, dynamic> json)
      : email = json['email'],
        password = json['password'],
        userName = json['username'];

  Map<String, dynamic> toJson() =>
      {'email': email, 'password': password, 'username': userName};

  Map<String, dynamic> toMap() {
    return {
      'email': email,
      'userName': userName,
      'password': password,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      email: map['email'],
      userName: map['userName'],
      password: map['password'],
    );
  }
}
