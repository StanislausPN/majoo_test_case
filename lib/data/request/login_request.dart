import 'dart:async';

import 'package:majootestcase/data/helper/database_helper.dart';
import 'package:majootestcase/data/models/user.dart';

class LoginRequest {
  DatabaseHelper databaseHelper = new DatabaseHelper();

  Future<User> getLogin(User user) {
    var result = databaseHelper.selectUser(user);
    return result;
  }
}
