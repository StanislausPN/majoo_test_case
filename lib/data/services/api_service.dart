import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/data/models/movie_response.dart';
import 'package:majootestcase/data/models/movie_title_model.dart';
import './dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieResponse?> getMovieList() async {
    try {
      var dio = await (dioConfig.dio() as Future<Dio>);
      Response<String> response = await dio.get(
          "https://imdb8.p.rapidapi.com/title/get-most-popular-movies/??homeCountry=US&purchaseCountry=US&currentCountry=US");
      // MovieTitleModels movieTitleModels =
      //     MovieTitleModels.fromJson(jsonDecode(response.data!));
      // MovieResponse movieResponse =
      //     MovieResponse.fromJson(jsonDecode(response.data));
      print('testing api');

      print(response);
      titleData.add(response.data!);
      print(titleData);
      // return movieTitleModels;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
