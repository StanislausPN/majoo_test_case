import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:majootestcase/data/models/move_details_model.dart';
import 'package:majootestcase/data/models/movie_response.dart';
import 'package:majootestcase/data/models/movie_title_model.dart';
import './dio_config_service.dart' as dioConfig;

class MovieDetailServices {
  Future<MovieDetails?> getMovieDetail(String titleId) async {
    try {
      var dio = await (dioConfig.dio() as Future<Dio>);
      Response<String> response = await dio.get(
          "https://imdb8.p.rapidapi.com//title/get-details?tconst=" + titleId);
      MovieDetails movieDetailsModel =
          MovieDetails.fromJson(jsonDecode(response.data!));
      // MovieResponse movieResponse =
      //     MovieResponse.fromJson(jsonDecode(response.data));
      print('testing api');
      print(response);
      return movieDetailsModel;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}
