import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/data/models/user.dart';
import 'package:majootestcase/data/response/login_response.dart';
import 'package:majootestcase/ui/common/widget/custom_button.dart';
import 'package:majootestcase/ui/common/widget/text_form_field.dart';

import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/register/register_page.dart';
import 'package:majootestcase/utils/constant.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> implements LoginCallBack {
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;

  late LoginResponse _response;

  _LoginState() {
    _response = LoginResponse(this);
  }

  // @override
  // void initState() {
  //   super.initState();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColors.mainColor,
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
              flex: 2,
              child: Padding(
                padding: const EdgeInsets.only(left: 20, top: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Align(
                        alignment: Alignment.topLeft,
                        child: GestureDetector(
                            onTap: () => Navigator.pop(context),
                            child: Icon(
                              Icons.arrow_back,
                              color: Colors.white,
                              size: 20,
                            )),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 24.0),
                      child: Text(
                        'Sign in',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 34,
                            color: Colors.white),
                      ),
                    ),
                  ],
                ),
              )),
          Expanded(
            flex: 8,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.horizontal(
                      left: Radius.circular(10), right: Radius.circular(10))),
              child: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.all(25),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 15),
                        child: Text(
                          'Welcome Back',
                          style: TextStyle(
                            fontSize: 30,
                            fontWeight: FontWeight.bold,
                            // color: colorBlue,
                          ),
                        ),
                      ),
                      Text(
                        'Please enter your email and password',
                        style: TextStyle(fontSize: 14, color: Colors.grey
                            // color: colorBlue,
                            ),
                      ),
                      SizedBox(
                        height: 9,
                      ),
                      _loginForm(),
                      SizedBox(
                        height: 50,
                      ),
                      Container(
                        width: double.infinity,
                        child: CustomButton(
                          text: 'Login',
                          onPressed: loginSubmit,
                          height: 100,
                          leadingIcon: Icon(
                            Icons.login,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      _registerButton(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _loginForm() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              if (EmailPattern.pattern.hasMatch(val!)) {
                return null;
              } else {
                return 'Masukkan e-mail yang valid';
              }
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            suffixIcon: IconButton(
              icon: Icon(
                _isObscurePassword
                    ? Icons.visibility_off_outlined
                    : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _registerButton() {
    return Align(
      alignment: Alignment.center,
      child: TextButton(
        onPressed: () async {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (_) => RegisterPage(),
            ),
          );
        },
        child: RichText(
          text: TextSpan(
              text: 'Don\'t have account ? ',
              style: TextStyle(color: Colors.black45),
              children: [
                TextSpan(
                  text: 'Register here',
                ),
              ]),
        ),
      ),
    );
  }

  void loginSubmit() async {
    final String? _email = _emailController.value;
    final String? _password = _passwordController.value;
    if (formKey.currentState?.validate() == true &&
        _email != null &&
        _password != null) {
      AuthBlocCubit authBlocCubit = AuthBlocCubit();
      User user = User(
        email: _email,
        password: _password,
      );
      authBlocCubit.loginUser(user, _response);
    }
  }

  @override
  void onLoginError(String error) {
    print(error);
  }

  @override
  void onLoginSuccess(User user) {
    print('login success');
    print(user.email);
    print(user.password);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (_) => BlocProvider(
          create: (context) => HomeBlocCubit()..fetchingData(),
          child: HomeBlocScreen(),
        ),
      ),
    );
  }
}
