import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/auth_bloc/auth_bloc_cubit.dart';
import 'package:majootestcase/bloc/home_bloc/home_bloc_cubit.dart';
import 'package:majootestcase/ui/common/widget/custom_button.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_screen.dart';
import 'package:majootestcase/ui/login/login_page.dart';
import 'package:majootestcase/ui/register/register_page.dart';
import 'package:majootestcase/utils/constant.dart';

class WelcomePage extends StatelessWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.mainColor,
        body: BlocListener<AuthBlocCubit, AuthBlocState>(
          listener: (context, state) {
            if (state is AuthBlocLoggedInState) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (_) => BlocProvider(
                    create: (context) => HomeBlocCubit()..fetchingData(),
                    child: HomeBlocScreen(),
                  ),
                ),
              );
            }
          },
          child: Column(
            children: [
              Expanded(
                  flex: 5,
                  child: Container(
                    height: 350,
                    width: 350,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('assets/cover.png'))),
                  )),
              Expanded(
                flex: 3,
                child: Container(
                  alignment: Alignment.bottomCenter,
                  margin: EdgeInsets.all(30),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 24.0),
                        child: Text(
                          'Welcome',
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 34,
                              color: Colors.white),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 24.0),
                        child: Text('IMDB Popular Movie List',
                            style:
                                TextStyle(fontSize: 24, color: Colors.white)),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 16.0),
                        child: CustomButton(
                          leadingIcon: Icon(
                            Icons.login,
                            color: Colors.white,
                          ),
                          onPressed: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => RegisterPage(),
                              )),
                          text: 'Create an account',
                        ),
                      ),
                      Align(
                        alignment: Alignment.center,
                        child: RichText(
                            text: TextSpan(children: [
                          TextSpan(
                              text: 'Already have an account? ',
                              style: TextStyle(
                                  fontSize: 14,
                                  color: Colors.grey[400]!.withOpacity(0.8))),
                          TextSpan(
                              text: 'Sign in',
                              style: TextStyle(
                                fontSize: 14,
                                color: Colors.white,
                              ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => LoginPage(),
                                      ));
                                })
                        ])),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ));
  }
}
