import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/data/helper/database_helper.dart';
import 'package:majootestcase/data/models/user.dart';
import 'package:majootestcase/data/response/login_response.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());

  void fetchHistoryLogin() async {
    emit(AuthBlocInitialState());
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    bool? isLoggedIn = sharedPreferences.getBool("is_logged_in");
    if (isLoggedIn == null) {
      emit(AuthBlocLoginState());
    } else {
      if (isLoggedIn) {
        emit(AuthBlocLoggedInState());
      } else {
        emit(AuthBlocLoginState());
      }
    }
  }

  void loginUser(User user, LoginResponse loginResponse) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    emit(AuthBlocLoadingState());
    String data = user.toJson().toString();
    print(data);
    sharedPreferences.setString("user_value", data);
    var userData = new User(email: user.email, password: user.password);
    var db = new DatabaseHelper();
    var userRetorno = new User(email: null, userName: null, password: null);
    userRetorno = await db.selectUser(userData);
    loginResponse.doLogin(user);
    if (userRetorno != null) {
      await sharedPreferences.setBool("is_logged_in", true);
      emit(AuthBlocLoggedInState());
    } else {
      print('gagal');
    }
  }
}
