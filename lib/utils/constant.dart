import 'dart:core';

import 'package:flutter/material.dart';

class Preference {
  static const USER_INFO = "user-info";
}

class Api {
  static const BASE_URL = "";
  static const LOGIN = "/login";
  static const REGISTER = "/register";
}

class Font {}

class ScreenUtilConstants {
  static const width = 320.0;
  static const height = 640.0;
}

class EmailPattern {
  static var pattern = RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
}

class AppColors {
  static const mainColor = Color(0xFF0041C4);
}
